package com.adprog.microservices.service.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;


@EnableZuulProxy
@EnableEurekaClient
@SpringBootApplication
public class GatewayServer {

    public static void main(String[] args) {
        // Tell Boot to look for gateway-server.yml
        System.setProperty("spring.config.name", "gateway-server");
        SpringApplication.run(GatewayServer.class, args);
    }
}