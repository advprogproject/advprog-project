package com.adprog.microservices.service.accounts.repository;

import java.util.List;
import com.adprog.microservices.service.accounts.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

	public Account findByName(String name);

	public List<Account> findByAddress(String address);

}
