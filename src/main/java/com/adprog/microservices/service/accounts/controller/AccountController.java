package com.adprog.microservices.service.accounts.controller;

import com.adprog.microservices.service.accounts.model.Account;
import com.adprog.microservices.service.accounts.repository.AccountRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AccountController {
    private final AccountRepository accountRepository;

    public AccountController(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @GetMapping("/getname")
    public Account getName(String name) {
        return accountRepository.findByName(name);
    }

    @PostMapping("/postaccount")
    public Account postCrash(@RequestBody Account account) {
        return accountRepository.save(account);
    }
}