package com.adprog.microservices.service.accounts.config;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;

import javax.sql.DataSource;
import org.apache.commons.dbcp2.BasicDataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;


@Configuration
@ComponentScan
@EnableJpaRepositories("com.adprog.microservices.service.accounts")
@EntityScan(basePackages = {"com.adprog.microservices.service.accounts"})
@PropertySource("classpath:application.properties")
public class AccountsConfiguration {

//     @Bean
//     public DataSource dataSource() throws URISyntaxException {
//         URI dbUri = new URI(System.getenv("DATABASE_URL"));

//         String username = dbUri.getUserInfo().split(":")[0];
//         String password = dbUri.getUserInfo().split(":")[1];
//         String dbUrl = "jdbc:postgresql://"
//                 + dbUri.getHost() + ':'
//                 + dbUri.getPort() + dbUri.getPath();

//         BasicDataSource basicDataSource = new BasicDataSource();
//         basicDataSource.setUrl(dbUrl);
//         basicDataSource.setUsername(username);
//         basicDataSource.setPassword(password);

//         System.out.println("In accounts");

//         return basicDataSource;

//     }

}
