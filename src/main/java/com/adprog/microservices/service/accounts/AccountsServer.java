package com.adprog.microservices.service.accounts;

// import com.adprog.microservices.common.config.DatabaseConfig;

import java.util.logging.Logger;

import com.adprog.microservices.service.accounts.config.AccountsConfiguration;
import com.adprog.microservices.common.config.DatabaseConfiguration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
@EnableDiscoveryClient
@Import({AccountsConfiguration.class, DatabaseConfiguration.class})
// @Import(DatabaseConfig.Class)
public class AccountsServer {

    @Autowired
//    private AccountRepository accountRepository;

    public static void main(String[] args) {
        // Will configure using accounts-server.yml
        System.out.println("Account Server running!");
        System.setProperty("spring.config.name", "accounts-server");
        SpringApplication.run(AccountsServer.class, args);
    }
}